<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 * @property string $fecha
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'texto', 'fecha'], 'required'],
            [['fecha'], 'safe'],
            [['titulo'], 'string', 'max' => 200],
            [['texto'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Codigo Entrada',
            'titulo' => 'Titulo',
            'texto' => 'Descripcion',
            'fecha' => 'Fecha de entrada',
        ];
    }
}
