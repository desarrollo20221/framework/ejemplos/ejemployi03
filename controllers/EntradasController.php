<?php

namespace app\controllers;

use app\models\Entradas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntradasController implements the CRUD actions for Entradas model.
 */
class EntradasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Entradas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(), // select * from entradas 
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entradas model.
     * @param int $id Codigo Entrada
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            // select * from Entradas where id = $id
            'model' => $this->findModel($id), 
            //'model' => Entradas::findOne(['id' => $id]),
        ]);
    }

    /**
     * Creates a new Entradas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        // creo un objeto de tipo entradas vacio
        $model = new Entradas();

        // si he apretado el boton de enviar
        if ($this->request->isPost) {
            if (
                $model->load($this->request->post()) // asignacion masiva al modelo de los datos escritos en el formulario
                && $model->save()) { // intenta almacenar el registro en la BBDD
                // aqui solo llega si el registro ya esta en la tabla de la BBDD
                // redirecciono la aplicacion a la accion view y le paso el id de la entrada generada
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        // cargando el formulario con el modelo de entradas generado
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entradas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id Codigo Entrada
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        // me creo un modelo con los datos de la entrada que tiene la id pasada como argumento
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entradas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id Codigo Entrada
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entradas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id Codigo Entrada
     * @return Entradas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entradas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('El registro no existe');
    }
    
    public function actionListar() {
        // crear un activeDataProvider
        $dataProvider = new ActiveDataProvider([
            'query' => Entradas::find(),
        ]);
        
        // llamo a la vista que tiene el gridview y le paso el dataProvider
        return $this->render('listar',[
            'datos' => $dataProvider,
        ]);
    }
}
